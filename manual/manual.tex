% Created 2018-09-26 Wed 21:40
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{dsfont}
\author{John Morris}
\date{\today}
\title{TickTock Clock Manual}
\hypersetup{
 pdfauthor={John Morris},
 pdftitle={TickTock Clock Manual},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.2.2 (Org mode 9.1.14)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Introduction}
\label{sec:org969b4ca}
Welcome to your tick-tock clock!  We hope you enjoy using it.

This clock is built of the following components:
\begin{description}
\item[{a wireless router}] running OpenWrt (GNU/Linux), this is the main
computer.  It provides the time, writes the display and hosts the
web interface
\item[{a serially driven display board}] see the schematics for more
information.  A set of shift-registers drive the LEDs
\item[{a voltage regulator}] adjustable between \textasciitilde{}4v and 5(.5)v: the ICs on
the display board are only supposed to run down to 4.5V, but I’ve
had them working down to at least 4.  Adjusting the voltage
allows adjusting the brightness of the main LEDs.  The digits may
be adjusted with the pot on the back.  Currently a linear lm317
regulator is used: this is inefficient and gets hot when driving
too many leds.  A switch-mode regulator is in the post to replace
it.
\item[{a stereo amplifier}] to drive the speakers
\item[{a usb hub}] as the router has one usb port
\item[{a memory stick}] which is the router’s hard disk
\item[{a usb sound card}] to play the chimes.
\end{description}

In operation the router board boots, gets the time from the internet,
and starts running the clock.  Obviously, to work it must be able to
connect to the wifi and the internet.

\section{Status of the Hardware}
\label{sec:org08d9012}
At the time of writing this document, the hardware is almost
complete.  It lacks the promised switch-mode regulator (still in the
post).

\section{Status of the Software}
\label{sec:org367fd46}
All functions are implemented.  The code is a hastily-patched version
of the mature, but not very pretty, code which runs my alarm clock; it
has been through a few conceptual changes and could really do with a
re-write.  However, it \emph{works}.  The main code is written in python;
the web code is written in PHP and interacts with a running instance
of the main code by means of a Unix \emph{named pipe} (FIFO) to which
commands can be sent.  This permits, if desired, the easy development
of a better (or at any rate prettier) web interface later.

\subsection{Getting the Code}
\label{sec:org39932ab}

If you want to read the code, it’s available (in all its horror) at
\url{https://gitlab.com/2e0byo/dads-clock}.  If you just need a backup
because something went wrong, I have a backup of the entire hdd I can
send.

\section{Using the Clock}
\label{sec:orge6045f3}

To begin with, plug it in!  After a few minutes the router will boot
and start the software; a few moments later it should sync its clock
over NTP and display the correct time.  It will stay in time as long
as the internet is up, automatically adjusting for daylight saving
time.  

\subsection{Telling the Time}
\label{sec:orgf8b7389}

This is the easy bit!  With the default display settings, the 24-hour
time can be simply read of the central digits (obviously).  The
bi-colour LEDs implement the hands, as follows:

\begin{description}
\item[{The Second Hand}] is the moving green dot (this one is easy)
\item[{The Minute Hand}] is the green dot which moves once per minute (duh)
and is \emph{probably} on a round tick unless the
minute is a multiple of 5.
\item[{The Hour Hand}] is (one of) the green square ticks.  It moves once
per hour, unlike a mechanical clock.
\end{description}

\subsection{Chiming}
\label{sec:org257ad73}

The Clock can chime like a grandfather clock.  The volume of this
chime can be adjusted like any other setting (see below).

\subsubsection{Enabling and Disabling Chiming}
\label{sec:orgce82a04}

Chiming can be toggled from the settings page (see below) or by a
short press on the \textbf{Left} button.

\subsubsection{Skipping a Chime}
\label{sec:org84a4848}

During a Chime, a short press on either button will cause the clock to
revert to whatever it was doing before.

\subsubsection{Chiming only during certain hours}
\label{sec:org6473972}

See \ref{sec:org9f31d67}

\subsection{Ticking}
\label{sec:org4bda6a2}

I really didn’t think you’d want this, so it’s not on by default.  But
if you want the thing to tick-tock (with a stereo tick moving with the
virtual pendulum, to boot), ticking can be toggled either by a short
press on the \textbf{Right} button or from the web interface.

\subsection{Alarm}
\label{sec:org9ed07f2}
The alarm code simply compares the time every second to the
closest alarm and triggers when they are the same, or the time is
ahead of the alarm (in case the clock has skipped a second).  Thus any
alarm set for a day when the clocks change will, unless set for the
moment of adjustment (in which case behaviour is not guaranteed),
simply elapse at the ‘new’ time: an alarm for 07h00 set the day
\emph{before} a move from GMT->BST will elapse at 07h00 BST, etc.

In the event of alarms triggering during running alarms (whether
ringing or snoozing) they will elapse as soon as the current alarm is
expired (not snoozed).

\subsubsection{Setting, Adjusting and Cancelling Alarms}
\label{sec:orgcb0b5ac}
Alarms can be set from the web interface, at
\url{http://ticktock.lan/clock/alarms.php}. Alarms may also be cancelled
from here; to adjust them it is currently necessary to delete and
re-create.  Alarms are one-shot only, for a specific date and time.
Recurring alarms can, if needed, be added to the code, but I wasn’t
expecting you to use them, so I haven’t.

\subsubsection{Snoozing and Stopping Alarms}
\label{sec:org10e626d}

\textbf{N.B.: When entering snooze mode the display may be corrupted.  This}
\textbf{is due to a bug yet to be fixed; just carry on as normal, pressing}
\textbf{Left or Right to adjust.  On the first press the display will be back}
*to normal.*i

Alarms may be snoozed by pressing the \textbf{Left} button.  The clock will
go into ‘set snooze’ mode, in which the central digits show ‘SNZ ’ or
some 7-seg approximation thereunto, and the number of illuminated
digits indicates the snooze period, which may be adjusted by pressing
the \textbf{Right} button to go up and the \textbf{Left} button to go down.  When
you are happy with the snooze period, press and hold either of the two
buttons down untill the central digits show the time and the hands
show a decreasing countdown to the elapsing of the snooze period.

Pressing the \textbf{Right} button results in the alarm immediately expiring
and the clock switching back into \emph{Clock Mode}.  (N.B. The ‘noisy’
flag which can be found in the code is a relic of my clock and is
ignored on this one.  If desired a saner system to start
chiming/ticking as required can be implemented).

A snooze may be cancelled, stopping the alarm altogether, by holding
down the \textbf{Left} button until the clock returns to \emph{Clock Mode}, or
edited, by holding down the \textbf{Right} button to re-enter ‘set snooze’
mode---exactly as when the alarm was ringing.

\subsubsection{Customising Alarm Playlist}
\label{sec:org36f66ac}

When an Alarm elapses the clock looks to see if the ‘wakeup’ playlist
contains anything.  If it does, it is loaded and the ‘alarm’ playlist
is appended, and then playback starts with the first track in ‘wakeup’
and procedes sequentially.  If ‘wakeup’ is empty, ‘alarm’ is loaded,
and tracks are played shuffled.  When the alarm finishes, ‘wakeup’ is
cleared.  From this we can see that there are two ways to customise
playback:

\begin{enumerate}
\item To set a special alarm as a once-off, add songs to the ‘wakeup’
playlist.

\item For perisistent changes, customise ‘alarm’.
\end{enumerate}

Obviously you can only add songs which are on the device.  To copy new
files over see \ref{sec:org347e12d}.  Once the files are
there, the mpd server needs to re-scan to find them. This may be done
from the web interface, but it may happen automatically.

\subsubsection{Controlling the MPD Server}
\label{sec:org3460dee}

Playback on the device is handled by the \texttt{MusicPlayerDaemon}, which is
a server which knows how to play files and read playlists.  Note that,
due to hardware limitations on the board, it can only decode \texttt{.flac}
files whilst reliably updating the display.

An interface to the server is provided at
\url{http://ticktock.lan/clock/mpd\_client.php}.  To illustrate its use, let
us walk through adding a song to the ‘wakeup’ playlist.  First we
delete the current ‘wakeup’ playlist by clicking the ‘d’ next to it:
\begin{center}
\includegraphics[width=.9\linewidth]{./screenshot1.png}
\end{center}
Then we add the desired songs to the play queue:
\begin{center}
\includegraphics[width=.9\linewidth]{./screenshot2.png}
\end{center}
Then we save the play queue as ‘wakeup’:
\begin{center}
\includegraphics[width=.9\linewidth]{./screenshot3.png}
\end{center}

It is important not to delete any of the playlists without replacing
it!  Doing so will cause the clock to fail to function.¬

\subsection{The Web Interface}
\label{sec:org2e24bc9}
Go to \url{http://ticktock.lan/clock/index.html} and figure it out from
there. 
\section{Configuring the Clock}
\label{sec:org29bc70a}
Most configuration options can be set in the web interface at
\url{http://ticktock.lan/clock/settings.php}. This isn’t pretty, but it
works.  It auto-loads with all the current settings, so you need only
worry about things you want to change.
\subsection{Display}
\label{sec:orgb684e92}

It is helpful to know how the display is built up in order to
customise it:

\begin{enumerate}
\item each ‘tick’ (minute division) is set to the colour of
\texttt{minor\_ticks}, which may be \texttt{green}, \texttt{red} or \texttt{off}.
\item each major tick (5-minute divisions) is set according to
\texttt{major\_ticks}.  This can be either \texttt{invert} (inverse colour of
\texttt{minor\_ticks}), \texttt{off} (set blank) or \texttt{False} = don’t display at
all.  If \texttt{minor\_ticks} = \texttt{off}, the inverse of \texttt{minor\_ticks\_pulse}
is used.
\item The Hour Hand is set, with the same logic for inversion if it falls
on an ‘off’ tick.
\item The Minute Hand is set, with the same logic for inversion if it falls
on an ‘off’ tick.
\item The Second Hand is set, with the same logic for inversion if it
falls on an ‘off’ tick.
\end{enumerate}

\subsubsection{Blanking}
\label{sec:org3b6383d}

Sometimes (for instance, when watching a film or sleeping) one doesn’t
want the display on.  To turn it off, either go to
\url{http://ticktock.lan/clock/settings.php} or hold down the \textbf{Right} button
untill the display turns off (hold down again to turn on again).  In
Blanked mode the clock functions as normal, only the display is
inhibited.  If an alarm elapses it will automatically switch the clock
face back on again; it is not possible to re-blank during snooze with
the buttons (as this would be interpreted as cancelling the snooze).
This is by design.

\subsection{Volume}
\label{sec:org8532aba}
A few different volumes may be set:

\begin{description}
\item[{\texttt{hardware\_volume}}] This is the souncard’s hardware volume setting,
between 0 and 100.  It set the maximum volume the clock can drive
the amplifier with.  You should only adjust it if you notice
clipping/distortion.
\item[{\texttt{chime\_volume}}] This is the percentage of the hardware volume which
the clock switches to before chiming.  Adjust it
to make chimes louder or softer.
\item[{\texttt{tick\_volume}}] This is the percentage of the hardware volume for
ticks.  Ditto.
\end{description}
\subsubsection{Day and Night Modes}
\label{sec:org9f31d67}

\textbf{N.B. Although the reading of a schedule has been implemented and}
\textbf{tested, the web interface is not yet complete.  Given its similarity}
\textbf{to the alarm shcedule, no problems are envisaged in writing it: I just}
\textbf{‘haven’t got round to it’ yet.}

It is envisaged that repeatedly turning chiming/ticking on and off may
be annoying.  Thus a basic scheduler is built in to enable the clock
to chime only between certain periods.  This scheduler can be adjusted
at \url{http://ticktock.lan/clock/daynight.php}

\subsection{Transferring and Accessing Files}
\label{sec:org347e12d}

The device is available over ssh.  I don’t envisage your copying much,
so I haven’t given you a web interface.  If you do need to access or
copy files (e.g. to add songs to the alarm or wakeup playlists), the
eaasiest method is to use a linux computer on the network (e.g. the
laptop in the back room we play DVDs from).  Open a terminal window
from the ‘start’ menu (under ‘system’) and run the command:

\texttt{scp /path/to/file/to/copy root@ticktock.lan:Music/}

which will ask for the password (on the slip of paper with the
clock).  As a hint, the path for a file called ‘song.flac’ copied
(dragged) over to the \emph{Desktop} of a linux computer would be
\texttt{\textasciitilde{}/Desktop/song.flac}.

To access the device itself, type \texttt{ssh root@ticktock.lan} into the
terminal and enter the password when prompted.

\section{Hardware Maintenance}
\label{sec:org64328e8}
I do not anticipate any serious need to open the case.  However, since
I dislike the omission of any information on how to do so which has
become all to common, I’ve written this section.

\subsection{Overview}
\label{sec:orgcbff87a}
The case is built from 5mm and 10mm perspex.  The main section is all
glued together; the screws in the rear were used in assembly to hold
the glued joints square, but should not be providing any strength.
The front panel is held a little off from the main case by four m6
bolts.  If desired it may be moved closer and these bolts trimmed
accordingly (mark them and remove them first!) but sufficient space
should be left for air to circulate, as some of the iternal components
get quite hot.

Although the case \textbf{looks} square it is very slightly rectangular, due
to errors from the perspex supplier.  (This also goes for the bandsaw
damage to the edges of many pieces: those I cut by hand are completely
clear).  Thus the front panel must be installed the right way up: if
the display board is removed it is advisable to mark the orientation
somehow.

\subsection{Removing the Front Panel}
\label{sec:org09ba3e4}

Remove the four M6 nuts and washers and slide the panel off.  The
display board is attaced to the power supply, brightness adjustment
pot and router board.  It is easiest to unplug the router board at the
router, but the others must be unplugged at the display board.  Note
the red dot indicating the polarity of the supply: do \textbf{not} get this
wrong!  Note also the orientation of the orange and white wires going
to the pot, as this latter is logarithmic and inverting them will make
the display harder to adjust.  Unplug all three connections and lay
the board to one side.  When unplugging be careful to support the
board and apply minimal force, as it is very thin (1mm) and flexible,
and flexing could damage tracks and/or joints.

\subsection{Removing Internal Components}
\label{sec:orgdee2489}

Unscrew as required.  Take care to remember \emph{how} each component was
mounted, and re-connect accordingly: plastic nuts, where used, are
essential, and must not be replaced with metal nuts for fear of
shorting connections.

\subsection{Reconnecting and Rebuilding}
\label{sec:orgf30205d}

*The display board will probably not survive a PSU polarity error.
Before powering up check (with a multimeter) the large copper ground
plane on the board is in continuity with the terminal marked ‘GND’ on
the regulator board.*

Screw down anything removed.  Connect power to the display board and
\textbf{check} it.  Connect the pot.  Power on the clock and wait \textasciitilde{}1 minute
for it to be fully booted.  Now connect the display board to the
router: if it works, you have the correct orientation of the display
connector.  If not, swap it round.  If it \textbf{still} fails to work,
unwrap the tape on the connector (the router board end) and re-solder
the dud connection.

Carefully slide the front panel over the four bolts.  You may have to
wiggle the board and washers a little to get it to sit properly.  Add
the outer washers and nuts.



\section{Troubleshooting}
\label{sec:org5d40084}
Remember how the hardware works: it needs a wifi signal, so if the
wifi goes down or the password changes, it’ll need reconfiguring
(email me).

It also needs an ntp server on the internet to stay in time: if it
displays the wrong time constantly, and the internet is up, the ntp
servers may have gone down (highly unlikely): email me for
instructions on how to update them.

This clock is built on the code and concept of my clock in Durham,
which has been continuously developed for three terms and is now
believed to be bug-free as I currently use it.  There is reason, then,
to hope that anything which does go wrong in the short term is down to
user error or unforseen events in the underlying os, and can be fixed
by following the following reboot procedure, assuming that \emph{the
network is in range and hasn’t been changed}:

\begin{itemize}
\item turn the clock off
\item turn the clock on again
\item wait for the display to come up.  If it is still broken at this
point, go to \url{http://ticktock.lan/clock/settings.php} and do a factory
reset, and then manually reboot the clock.
\end{itemize}

if it \emph{still} doesn’t work, email me.
\end{document}