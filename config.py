#!/usr/bin/python3
"""Basic runtime configuration editor.  Supports run-time and
load-time editing of variables in variables.py ONLY; all other
requests ignored.  Uses two FIFOs, one at /tmp/clock_in.fifo and the
other at /tmp/clock_out.fifo
"""

from variables import variables, variable_types, default_mode
from display import toggle_blank_display

from os import mkfifo, path
import jsonpickle
from grandfather_clock import toggle_chime
from tick import toggle_tick


in_fifo = "/tmp/clock_in.fifo"
out_stream = "/tmp/clock.stream"
conffile = "/root/.clock_config"


def init():
    for i in [in_fifo]:
        if not path.exists(i):
            mkfifo(i)
    for i in [out_stream, conffile]:
        if not path.exists(i):
            open(i, "a").close()


def write_out(s):
    print(s)
    if not s.endswith('\n'):
        s += '\n'
    with open(out_stream, "a") as f:
        f.write(s)


def get_var(var):
    """Try to print variable if exists"""
    try:
        write_out(variables[var] + "\n")
    except NameError:
        write_out("No such variable: %s\n" % var)


def set_var(var, value, save=True):
    """Try to set variable if exists, saving in config file for
persistence if save=True (default)"""
    try:
        variables[var] = value
        write_out('Successfully set %s to %s' % (var, value))
    except KeyError:
        write_out("No such variable: %s\n" % var)
        return
    if save is True:
        print('saving')
        save_config(variables)


def save_config(confs):
    """save all key=value pairs in confs to config file"""
    try:
        current = jsonpickle.decode(open(conffile, "r").read())
    except:
        current = {}
    for key in confs:
        current[key] = confs[key]
    with open(conffile, "w") as f:
        f.writelines([
            jsonpickle.encode(current) + '\n',
            jsonpickle.encode(variable_types)
        ])


def load_config():
    """load saved config"""
    try:
        saved = jsonpickle.decode(open(conffile, "r").readlines()[0])
    except:
        saved = {}
    for key in saved:
        variables[key] = saved[key]


def read_fifo_loop():
    """watch fifo and parse each line for commands when written: blocking"""
    while True:
        with open(in_fifo, 'r') as fifo:
            #  select.select([fifo],[],[fifo])
            fifo_open = True
            commands = []
            while fifo_open is True:
                lines = fifo.readlines()
                if len(lines) == 0:
                    fifo.close()
                    fifo_open = False
                else:
                    commands += lines
        for command in commands:
            command = command.rstrip().lstrip().split()
            if command[0].upper() == "SET":
                try:
                    set_var(
                        command[1],
                        command[2],
                        save=command[3] if len(command) > 3 else True)
                except:
                    write_out('invalid command')
            elif command[0].upper() == "RESET":
                with open(conffile, 'w') as f:
                    f.write("")
                default_mode()
                save_config(variables)
            elif (command[0].upper() == "TOGGLE") and (
                    command[1].upper() == "BLANK"):
                toggle_blank_display()
            elif (command[0].upper() == "TOGGLE") and (
                    command[1].upper() == "CHIME"):
                toggle_chime()
            elif (command[0].upper() == "TOGGLE") and (
                    command[1].upper() == "TICK"):
                toggle_tick()

            else:
                write_out('invalid command')
                continue
