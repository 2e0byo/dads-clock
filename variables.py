variables = {
    # "minor_ticks": 'off',  # 'red', 'green', 'off'
    # "minor_ticks_pulse": 'green',
    # # 'red', 'green'; ignored unless minor_ticks  :  'off', then the colour to pulse
    # "major_ticks": 'invert',  # 'invert', 'off', 'False' : don't display
    # "hour_hand": 'invert',  # 'invert', 'off', 'False' : don't display
    # "minute_hand": 'invert',  # 'invert', 'off', 'False' : don't display
    # "second_hand": 'invert'  # 'invert', 'off', 'False' : don't display
}
"""List of types for each variable and allowed values.  Hierarchy:
variable_types[key] = {'vtype': TYPE, 'values': [VALUES]}
where TYPE is one of
   * 'string'
   * 'int'
   * 'list' (of strings as required)
"""
variable_types = {}


def add_variable(variable, default, variable_type):
    """Short-hand to add a variable"""
    variables[variable] = default
    variable_types[variable] = variable_type


def default_mode():
    for i in ['minor_ticks']:
        add_variable(i, 'off',
                     {'vtype': 'list',
                      'values': ['off', 'red', 'green']})
    for i in ['minor_ticks_pulse']:
        add_variable(i, 'green', {'vtype': 'list', 'values': ['red', 'green']})
    for i in ['major_ticks', 'hour_hand', 'minute_hand', 'second_hand']:
        add_variable(i, 'invert',
                     {'vtype': 'list',
                      'values': ['invert', 'off', 'False']})
    add_variable('chime_volume', "60",
                 {'vtype': 'int',
                  'values': ['0', '100']})
    add_variable('tick_volume', "30",
                 {'vtype': 'int',
                  'values': ['0', '100']})

    add_variable('hardware_volume', "60",
                 {'vtype': 'int',
                  'values': ['0', '100']})




def plain_mode():
    d = {
        'minor_ticks': 'off',  # 'red', 'green', 'off'
        'minor_ticks_pulse':
        'green',  # 'red', 'green'; ignored unless minor_ticks = 'off', then the colour to pulse
        'major_ticks': 'invert',  # 'invert', 'off', 'False'=don't display
        'hour_hand': 'invert',  # 'invert', 'off', 'False'=don't display
        'minute_hand': 'invert',  # 'invert', 'off', 'False'=don't display
        'second_hand': 'invert',  # 'invert', 'off', 'False'=don't display
    }
    for key in d:
        variables[key] = d[key]


def fancy_mode():
    d = {
        'minor_ticks': 'green',  # 'red', 'green', 'off'
        'minor_ticks_pulse':
        'green',  # 'red', 'green'; ignored unless minor_ticks = 'off', then the colour to pulse
        'major_ticks': 'invert',  # 'invert', 'off', 'False'=don't display
        'hour_hand': 'invert',  # 'invert', 'off', 'False'=don't display
        'minute_hand': 'invert',  # 'invert', 'off', 'False'=don't display
        'second_hand': 'off',  # 'invert', 'off', 'False'=don't display
    }
    for key in d:
        variables[key] = d[key]
