#!/usr/bin/python
from re import findall
from subprocess import call, check_output, CalledProcessError, Popen
from time import time, sleep

import flags
from variables import variables
import tick
from log import log
from mpd_client import play, stop
from display import leds
chime_dir = "/root/chimes"
aplay_args = "--buffer-size=8192 -q"


def set_volume(volume):
    call(["amixer", "-q", "set", "Speaker", volume])


def set_save(volume):  # set volume to safe limit and then restore
    current_volume = check_output(["amixer", "get", "Speaker"])
    current_volume = findall(r'(\[[0-9][0-9]+\%\])',
                             current_volume)[0].strip('[]')
    set_volume(volume)
    return (current_volume)


def aplay_file(aplay_args, afile):
    try:
        call(["aplay", aplay_args, afile])
    except CalledProcessError:
        log('aplay ' + aplay_args + ' ' + afile + ' failed...')
        return  # prevent loops


def popen_aplay_file(aplay_args, afile):
    """Start aplay on file with Popen.  Return the PopenObject if
successful, else return False"""
    try:
        toll_process = Popen(["aplay", aplay_args, afile])
        return (toll_process)
    except OSError:
        return (False)


def chime(mins, hours):
    flags.ChimeFlag = True
    start = time()
    if mins < 7.5:
        play('hours', int(hours) - 1, int(variables['chime_volume']))
    elif 7.5 < mins < 22.5:
        play('1q', 0, int(variables['chime_volume']))
    elif 22.5 < mins < 37.5:
        play('1h', 0, int(variables['chime_volume']))
    elif 37.5 < mins < 52.5:
        play('3q', 0, int(variables['chime_volume']))
    elif mins > 52.5:
        play('hours', int(hours) - 1, int(variables['chime_volume']))

    if flags.ChimeFlag is False:
        return  # killed externally, e.g. by alarm
    if flags.TickFlag is True:
        tick.start_tick()
    end = time()
    #    print (end-start)
    if (end - start) < 60:
        sleep(60 + start - end)  # make sure we don't double-play
    flags.ChimeFlag = False


def stop_chime():
    if flags.ChimeFlag is True:
        stop()
    flags.ChimeFlag = False


def chime_on():
    leds['chime'] = True

def toggle_chime():
    if leds['chime'] is False:
        leds['chime'] = True
    else:
        leds['chime'] = False
        stop_chime()
