#!/usr/bin/python3

from display import init_gpio, write_time
from time import time, sleep
from datetime import datetime
from os import system
from threading import Thread
import config
from variables import variables, default_mode


def basic_clock():
    print('started clock')
    while True:
        start = time()
        Thread(target=write_time, args=(datetime.now(),)).start()
        try:
            sleep(1 + start - time())
        except:  # we get a bunch of errors.  Sometime analyse them
            # and clean this up.
            print('Skipped.  If this keeps happening, check system load')
            system(
                "logger 'Alarm clock skipped, if this keeps happening, check system\
load'")


init_gpio()
default_mode()                    # init variables
config.init()
config.load_config()
config.save_config(variables)     # full config file
Thread(target=config.read_fifo_loop).start()
basic_clock()
