#!/usr/bin/python3

from time import sleep

from periphery import GPIO
from variables import variables
from threading import Thread
import flags

sclk = 497
sdata = 511
latch = 498
serial_delay = 0.002

# --------------------------------------------------------------------
# bitbanging
# --------------------------------------------------------------------


def init_gpio():
    global sclk_gpio, sdata_gpio, latch_gpio
    sclk_gpio = GPIO(sclk, "out")
    sdata_gpio = GPIO(sdata, "out")
    latch_gpio = GPIO(latch, "out")


def serial_out(data, strobe=True):
    """serial out by writing to gpios"""
    for bit in data:
        sdata_gpio.write(bit)
        sleep(serial_delay)
        sclk_gpio.write(False)
        sleep(serial_delay)
        sclk_gpio.write(True)
    if strobe is True:
        sleep(serial_delay)
        latch_gpio.write(True)
        sleep(serial_delay)
        latch_gpio.write(False)


# --------------------------------------------------------------------
# 7seg
# --------------------------------------------------------------------

# --------------------------------------------------------------------
# 7-seg font
# --------------------------------------------------------------------

font = {
    '0': [True, True, True, True, True, True, False],
    '1': [False, False, False, False, True, True, False],
    '2': [True, True, False, True, True, False, True],
    '3': [True, False, False, True, True, True, True],
    '4': [False, False, True, False, True, True, True],
    '5': [True, False, True, True, False, True, True],
    '6': [True, True, True, True, False, True, True],
    '7': [False, False, False, True, True, True, False],
    '8': [True, True, True, True, True, True, True],
    '9': [True, False, True, True, True, True, True],
    'a': [False, True, True, True, True, True, True],
    'b': [True, True, True, False, False, True, True],
    'c': [True, True, False, False, False, False, True],
    'd': [True, True, False, False, True, True, True],
    'e': [True, True, True, True, False, False, True],
    'f': [False, True, True, True, False, False, True],
    'g': [True, False, True, True, True, True, True],
    'h': [False, True, True, False, False, True, True],
    'i': [False, True, True, False, False, False, False],
    'j': [True, False, False, False, True, True, False],
    'k': [False, True, True, False, False, False, True],
    'l': [True, True, True, False, False, False, False],
    'm': [False, True, False, False, False, True, True],
    'n': [False, True, False, False, False, True, True],
    'o': [True, True, False, False, False, True, True],
    'p': [False, True, True, True, True, False, True],
    'q': [False, False, True, True, True, True, True],
    'r': [False, True, False, False, False, False, True],
    's': [True, False, True, True, False, True, True],
    't': [False, True, True, True, False, False, False],
    'u': [True, True, False, False, False, True, False],
    'v': [True, True, False, False, False, True, False],
    'w': [True, True, False, False, False, True, False],
    'x': [False, False, False, False, False, False, True],
    'y': [False, False, True, False, True, True, True],
    'z': [True, True, False, True, True, False, True],
    ' ': [False, False, False, False, False, False, False]
}

# --------------------------------------------------------------------
# Leds
# --------------------------------------------------------------------

leds = {'dots': True, 'chime': True, 'alarm': False, 'tick': False}
led_per_digits = {0: 'chime', 1: 'dots', 2: 'dots', 3: 'alarm'}


def write_digits(digits):
    """write digits (before writing leds)  Right-justifies if need be"""
    if flags.DisplayBlanked is True:
        serial_out([False] * 8 * 4)
        return
    digits = list(digits)  # so we can write strings most of the time
    digits.reverse()
    digits += ["0", "0", "0", "0"]
    for digit_index in range(2):
        serial_out(
            reversed(font[digits[digit_index]] +
                     [leds[led_per_digits[digit_index]]]),
            strobe=False)
    for digit_index in range(2, 4):
        i = font[digits[digit_index]]
        j = [i[3], i[4], i[5], i[0], i[1], i[2], i[6]]  # invert
        serial_out(
            reversed(j + [leds[led_per_digits[digit_index]]]), strobe=False)


def write_display(digits, ticks):
    """write out all ticks and digits"""
    if flags.DisplayBlanked is True:
        ticks = [False, False] * 60
        digits = '    '         # catch as catch can
#    write_digits(digits)
#    serial_out(reversed(ticks))
    Thread(target=write_both, args=(digits, ticks)).start()


def write_both(digits, ticks):
    flags.display_lock = True
    write_digits(digits)
    serial_out(reversed(ticks))
    flags.display_lock = False


# --------------------------------------------------------------------
# hands and ticks
# --------------------------------------------------------------------

red_green = {
    'red': [False, True],
    'green': [True, False],
    'off': [False, False]
}


def set_ticks(ticks, positions, value):
    """set all ticks at positions (a list) to value, where value is one of
'invert', 'off', False"""
    if value == 'False':
        return (ticks)
    for p in positions:
        current = ticks[p * 2:p * 2 + 2]
        if value == 'invert':
            if current != [False, False]:
                current = reversed(current)
            # if minor_ticks off, turn on with colour minor_ticks_pulse
            elif variables['minor_ticks'] == 'off':
                current = red_green[variables['minor_ticks_pulse']]
                if p % 5 == 0 and variables['major_ticks'] == 'invert':
                    current = reversed(current)
            # else turn on with minor_ticks colour
            elif p % 5 == 0 and variables['major_ticks'] == 'invert':
                current = reversed(red_green[variables['minor_ticks']])
            else:
                current = red_green[variables['minor_ticks']]

        elif value == 'off':
            current = [False, False]
        ticks[p * 2:p * 2 + 2] = current
    return (ticks)


def write_time(t):
    """Write the hands and numbers for time t (datetime.datetime object)"""
    # 60 ticks of base colour
    ticks = red_green[variables['minor_ticks']] * 60
    # set major ticks (5s)
    ticks = set_ticks(ticks, list(range(0, 60, 5)), variables['major_ticks'])
    # set hour hand
    hour = int(t.strftime('%I'))
    if hour == 12:  # don't know if this happens
        hour = 0
    hour = hour * 5
    ticks = set_ticks(ticks, [hour], variables['hour_hand'])
    # minute hand
    minute = int(t.strftime('%M'))
    if not all([
            minute == hour, variables['minute_hand'] == variables['hour_hand']
    ]):  # don't print if overlapping
        ticks = set_ticks(ticks, [minute], variables['minute_hand'])
    # second hand
    second = int(t.strftime('%S'))
    ticks = set_ticks(ticks, [second], variables['second_hand'])

    # write out display
    write_display(t.strftime("%H%M"), ticks)


def write_quantity(quantity, digits):
    """Write a quantity to the outer LEDs, either between 0 and 60 or
between 0 and 120.  Display the contents of 'digits' on the inner
digits.  Intended for e.g. setting the volume."""
    if quantity > 120:
        raise ValueError('Tried to display quantity bigger than 120')
    if quantity < 61:
        ticks = (red_green['green'] * quantity) + (
            red_green['off'] * (60 - quantity))
    else:
        ticks = (red_green['red'] * (quantity // 2)) + (
            red_green['green'] * (quantity % 2)) + (red_green['off'] * (60 -
                                                  (quantity // 2) -
                                                  (quantity % 2)))
    write_display(digits, ticks)


def blank_display():
    flags.DisplayBlanked = True


def unblank_display():
    flags.DisplayBlanked = False


def toggle_blank_display():
    if flags.DisplayBlanked is True:
        unblank_display()
    else:
        blank_display()
