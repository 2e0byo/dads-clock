#!/usr/bin/python
import signal
import sys
import traceback
import csv
from datetime import datetime, timedelta
from dateutil.parser import parse
from os import getpid, path, stat, system
from threading import Thread
from time import sleep, strftime, time

import alarm
import mpd_client
import grandfather_clock
import input_gpio
import flags
from display import init_gpio, leds, write_time, write_quantity, toggle_blank_display
from log import log, logf, test_print
from variables import variables, default_mode
import config
from grandfather_clock import stop_chime, toggle_chime, chime_on
from tick import stop_tick, start_tick, toggle_tick
from restart_ntp import restart_ntp

# hardcoded variables
alarmfile = '/opt/alarms'
schedulefile = '/opt/schedule'
alarms = []
elapsed = 0
file_modification_dates = {}
interactive_string = ""  # prompt for interactive mode
global_alarm_list = []
global_noisy_list = []
current_mode = ""
last_mode = ""
pre_alarm_show_mode = ""
current_alarm_index = 0
alarm_file_lock = False

log.debug_mode = False


def do_nothing():
    pass


# --------------------------------------------------------------------
# Dump/Reload Config
# --------------------------------------------------------------------


def dump_config():
    # flags
    logf('/tmp/dump', 'flags:')
    for i in [i for i in vars(flags) if not i.startswith('__')]:
        logf('/tmp/dump', [i, vars(flags)[i]])
    logf('/tmp/dump', 'locals:')
    logf('/tmp/dump', locals())
    logf('/tmp/dump', "globals:")
    logf('/tmp/dump', globals())


def signal_handler(signal, frame):
    dump_config()


# --------------------------------------------------------------------
# File Functions
# --------------------------------------------------------------------


def poll_file(path):  # relies on global dictionary
    try:
        modtime = stat(path)[8]
    except OSError:
        return False  # path doesn't exist
    try:
        oldmodtime = file_modification_dates[path]
    except KeyError:
        file_modification_dates[path] = modtime
        return True  # first time looking
    if modtime != oldmodtime:
        file_modification_dates[path] = modtime
        return True  # modified
    else:
        return False  # unmodified


# --------------------------------------------------------------------
# Interactive functions
# --------------------------------------------------------------------
def interactive_up():
    """Increment user_input by 1 if less than interactive_upper_limit and
redisplay"""
    if flags.user_input < flags.interactive_upper_limit:
        flags.user_input += 1
        interactive_display()


def interactive_down():
    """Decrement user_input by 1 if greater than interactive_lower_limit
and redisplay"""
    if flags.user_input > flags.interactive_lower_limit:
        flags.user_input -= 1
        interactive_display()


def interactive_exit():
    """Exit interactive input mode"""
    flags.interactive = False


def interactive_display():
    """Redisplay interative_string + current value of user_input"""
    global interactive_string
    leds['dots'] = False
    write_quantity(int(flags.user_input), interactive_string)


def interactive_mode():
    """Enter interactive input mode"""
    new_mode('interactive')
    flags.async_display = True  # prevent clashes
    flags.interactive = True
    flags.functions["left_button_long"] = interactive_exit
    flags.functions["left_button_short"] = interactive_down
    flags.functions["right_button_short"] = interactive_up
    flags.functions["right_button_long"] = interactive_exit
    while flags.display_lock is True:  # display in progress
        sleep(0.1)
    interactive_display()



def async_refresh_display():
    if flags.async_display is True:
        return  # async display in progress
    if flags.display_lock is True:  # display in progress
        sleep(0.4)
    flags.async_display = True
    flags.display_function()
    flags.async_display = False


# --------------------------------------------------------------------
# Alarm Functions
# --------------------------------------------------------------------


def stop_alarm(snoozing=False):
    global elapsed
    elapsed = 0
    alarm.stop_running_alarm(snoozing)
    clock_mode()


def check_alarm():
    """If not inhibited by ongoing snooze or alarm, check for
alarms in alarmfile.  If alarm in progress check for elapsed snooze"""
    global alarms, elapsed
    ring = False  # don't initiate ring
    if flags.interactive is True:  # in interactive input mode
        return
    if flags.AlarmFlag is True:
        elapsed += 1
        if flags.snooze is not False:
            flags.snooze -= 1
            if flags.snooze != 0:
                return
            else:
                # print('snooze elapsed')
                ring = True

    if alarm_file_lock is True:
        return
    global alarmfile
    alarm_list = []
    if (path.isfile(alarmfile)) and (path.getsize(alarmfile) > 0):
        leds['alarm'] = True
        modified = False
        with open(alarmfile, 'r') as f:
            alarms = f.readlines()
        for entry in alarms:
            try:
                entry_time = entry
                alarmtime = parse(entry_time.strip('\n'))
                if alarmtime not in alarm_list:
                    alarm_list.append(alarmtime)
                if datetime.now() - alarmtime >= timedelta(minutes=0):
                    flags.AlarmFlag = True
                    modified = True
                    ring = True
                    alarms.remove(entry)
                    elapsed = 0  # reset alarm count
            except ValueError:
                pass
        if modified is True:
            with open(alarmfile, 'w') as f:
                for entry in alarms:
                    f.write(entry)
    else:
        leds['alarm'] = False
    if ring is True:  # ring
        # print('ringing')
        alarm_mode()
        Thread(target=alarm.play_alarm()).start()
    global global_alarm_list
    alarm_list.sort()
    global_alarm_list = alarm_list


def set_snooze():
    """Interactively set a snooze alarm for min minutes into the future"""
    global interactive_string
    stop_alarm(snoozing=True)  # stop ringing---leaves us in clock_mode
    interactive_string = 'snze'
    interactive_mode()
    interactive_display()  # try to cure bad displaying

    #    timeout = 1500
    timeout = 300
    while (flags.interactive is True) and (timeout > 0):
        sleep(0.2)  # wait for user input
        timeout -= 1
    interactive_exit()  # in case timed out

    mins = flags.user_input
    if mins == 0:
        flags.snooze = 1  # trigger immediately, to prompt for
        # correction
    else:
        flags.snooze = mins * 60  # seconds
    alarm_mode()
    flags.AlarmFlag = True  # inhibit anything else in snooze!
    flags.async_display = False
    flags.display_function = display_snooze


def background_set_snooze():
    Thread(target=set_snooze).start()


def display_alarm():
    global elapsed
    if elapsed < 61:
        write_quantity(elapsed, strftime('%H%M'))
        return
    elapsed_mins = elapsed // 60
    write_quantity(elapsed_mins if elapsed_mins < 121 else 120,
                   strftime('%H%M'))


def display_snooze():
    if flags.snooze >= 60:
        snz = flags.snooze // 60
        write_quantity(snz if snz < 121 else 120, strftime('%H%M'))
    else:
        write_quantity(flags.snooze, strftime('%H%M'))


# --------------------------------------------------------------------
# Show and edit alarm times
# --------------------------------------------------------------------


def alarm_show_mode():
    """Asynchronous mode.  Cycle through alarms.  Long press to edit, both
to exit.  Times out."""
    flags.async_display = True
    new_mode('alarm_show')
    flags.display_function = do_nothing
    leds['dots'] = True
    flags.functions["left_button_long"] = edit_current_alarm
    flags.functions["left_button_short"] = show_previous_alarm
    flags.functions["right_button_short"] = show_next_alarm
    flags.functions["right_button_long"] = edit_current_alarm
    flags.functions["both_buttons"] = exit_alarm_show_mode


def show_edit_alarms():
    """must return quickly as blocks further button input"""
    global global_alarm_list
    global pre_alarm_show_mode, current_mode
    pre_alarm_show_mode = current_mode
    Thread(target=background_flash_alarm).start()


def background_flash_alarm():
    if len(global_alarm_list) == 0:
        write_quantity(0, 'none')
        sleep(2)  # crude 'flash'
        return
    alarm_show_mode()
    show_nth_alarm(0)
    if flags.InteractiveAlarmFlag is False:
        exit_alarm_show_mode()
        test_print('auto returning flash')


def exit_alarm_show_mode():
    flags.InteractiveAlarmFlag = False
    global pre_alarm_show_mode
    switch_mode(pre_alarm_show_mode)
    flags.async_display = False


def show_nth_alarm(n, blocking=True):
    global global_alarm_list, current_alarm_index
    current_alarm_index = n
    if blocking is False:
        t = 0
    else:
        t = 2
    if len(global_alarm_list) == 0:
        write_quantity(0, 'none')
        sleep(2)  # crude 'flash'
        return
    next_alarm = global_alarm_list[n]

    write_quantity((next_alarm.date() - datetime.now().date()).days,
                   next_alarm.strftime('%H%M'))
    sleep(t)


def show_next_alarm():
    flags.InteractiveAlarmFlag = True
    global global_alarm_list, current_alarm_index
    i = current_alarm_index + 1
    if i >= len(global_alarm_list):
        i = 0  # roll over
    show_nth_alarm(i, blocking=False)


def show_previous_alarm():
    flags.InteractiveAlarmFlag = True
    global global_alarm_list, current_alarm_index
    i = current_alarm_index - 1
    if i < 0:
        i = len(global_alarm_list) - 1  # roll over
    show_nth_alarm(i, blocking=False)


def edit_current_alarm():
    flags.InteractiveAlarmFlag = True
    # let us know we got there, into dangerous ground
    write_quantity(0, '    ')
    sleep(0.1)  # get attention
    alarm_edit_mode()
    show_nth_alarm(current_alarm_index, blocking=False)


# --------------------------------------------------------------------
# Edit Alarm Functions
# --------------------------------------------------------------------


def alarm_edit_mode():
    new_mode('alarm_edit')
    flags.async_display = True
    flags.display_function = do_nothing
    leds['dots'] = True
    flags.functions["left_button_long"] = delete_current_alarm
    flags.functions["left_button_short"] = alarm_down_minute
    flags.functions["right_button_short"] = alarm_up_minute
    flags.functions["right_button_long"] = alarm_up_hour
    flags.functions["both_buttons"] = exit_alarm_edit_mode


def exit_alarm_edit_mode():
    flags.InteractiveAlarmFlag = False
    alarm_show_mode()
    write_quantity(0, '    ')
    sleep(0.1)  # get attention
    show_nth_alarm(0, blocking=False)  # move to beginning of list


def alarm_up_minute():
    """increment current alarm by 1 minute"""
    global current_alarm_index
    alarm_up_down(current_alarm_index, 1)


def alarm_up_hour():
    """increment current alarm by 1 hour"""
    global current_alarm_index
    alarm_up_down(current_alarm_index, 60)


def delete_current_alarm():
    """delete current alarm from global_alarm_list, re-write file, exit
alarm_edit_mode"""
    global current_alarm_index, global_alarm_list
    del (global_alarm_list[current_alarm_index])
    global alarm_file_lock, alarmfile

    alarm_file_lock = True  # nobody else is to edit anything...
    with open(alarmfile, 'w') as f:
        for entry in global_alarm_list:
            f.write(entry.strftime("%A %d %B %Y %H:%M:%S") + "\n")
    alarm_file_lock = False
    exit_alarm_edit_mode()


def alarm_down_minute():
    """decrement current alarm by 1 minute"""
    alarm_up_down(current_alarm_index, -1)


def alarm_up_down(alarm_index, mins):
    """Lock the alarm file, increment the alarm in global_alarm_list at
alarm_index by mins minutes (if negative decrement) and then wholly
replace the file with the contents of global_alarm_list.  Note this is
dangerous: it needs a timeout!  Note that using this will strip
'noisy' flags (because I'm currently sufficiently tired it seems
nontrivial to cope with them).  When finished redisplay current alarm
time.

    """
    global alarm_file_lock, alarmfile
    global global_alarm_list
    alarm_file_lock = True  # nobody else is to edit anything...
    global_alarm_list[alarm_index] += timedelta(minutes=mins)
    with open(alarmfile, 'w') as f:
        for entry in global_alarm_list:
            f.write(entry.strftime("%A %d %B %Y %H:%M:%S") + "\n")
    alarm_file_lock = False
    show_nth_alarm(alarm_index, blocking=False)


# --------------------------------------------------------------------
# Chime Functions
# --------------------------------------------------------------------


def check_chime():
    if leds['chime'] is False:  # Chime off
        return
    if flags.interactive is True:  # in interactive input mode
        return
    if flags.ChimeFlag is True:  # already chiming or waiting for
        # minute to elapse
        return
    if flags.AlarmFlag is True:  # running/snoozed alarm
        return

    nowm = int(strftime('%M'))
    nowh = strftime('%I')

    if (nowm % 15 == 0):
        if leds['tick'] is True:
            stop_tick()
        flags.ChimeFlag = True  # moved to prevent race condition
        Thread(
            target=grandfather_clock.chime, args=(nowm, nowh)).start()  # chime


# --------------------------------------------------------------------
# Tick Functions
# --------------------------------------------------------------------


def check_tick():
    """Check if tick has been stopped by something else and should be
re-enabled, and if so start it"""
    if flags.AlarmFlag is True:
        return
    if flags.ChimeFlag is True:
        return
    if flags.TickFlag is True and flags.TickOn is False:
        # print 'restarting tick'
        start_tick()


# --------------------------------------------------------------------
# Modes
# --------------------------------------------------------------------


def new_mode(mode):
    """update current and last mode"""
    global last_mode
    global current_mode
    last_mode = current_mode
    current_mode = mode


def switch_last_mode():
    """Exit current mode and go back to the last one"""
    global last_mode
    global current_mode
    last_mode = current_mode
    switch_mode(current_mode)


def switch_mode(mode):
    if mode == 'clock':
        clock_mode()
    elif mode == 'alarm':
        alarm_mode()
    elif mode == 'interactive':
        interactive_mode()
    elif mode == 'alarm_edit':
        alarm_edit_mode()
    elif mode == 'alarm_show':
        alarm_show_mode()


def clock_mode():
    new_mode('clock')
    flags.display_function = display_time
    leds['dots'] = True
    flags.functions["left_button_long"] = toggle_chime
    flags.functions["left_button_short"] = toggle_chime
    flags.functions["right_button_short"] = toggle_tick
    flags.functions["right_button_long"] = toggle_blank_display
    flags.functions["both_buttons"] = show_edit_alarms


def alarm_mode():
    new_mode('alarm')
    stop_chime()
    stop_tick()
    flags.display_function = display_alarm
    leds["dots"] = True
    flags.functions["left_button_long"] = background_set_snooze
    flags.functions["left_button_short"] = background_set_snooze
    flags.functions["right_button_short"] = stop_alarm
    flags.functions["right_button_long"] = stop_alarm
    flags.functions["both_buttons"] = show_edit_alarms


def display_time():
    write_time(datetime.now())


# --------------------------------------------------------------------
# Day and Night Scheduler
# --------------------------------------------------------------------

schedule = {}


def read_and_follow_schedule(schedulefile):
    """Check schedule for command.  If command has elapsed and flag not set, execute command and set flag"""
    # check for command from scheduler
    if (path.isfile(schedulefile)) and (path.getsize(schedulefile) > 0):
        if (poll_file(schedulefile) is True):
            with open(schedulefile, 'r') as f:
                reader = csv.DictReader(f)
                schedule.clear()     # reset!
                for row in reader:
                    schedule[row["time"]] = {
                        "days": row["[days list]"],
                        "command": row["command"]
                    }

    else:
        return  # don't used cached version if currently re-writing!

    # use ram version of schedule to check for an alarm
    for t in schedule:
        if datetime.now().strftime("%H:%M") in t and datetime.now().strftime(
                "%A").upper() in schedule[t]["days"].upper():
            command = schedule[t]["command"]
            if command == "chime_on" and flags.schedule_chime_on_flag is False:
                leds['chime'] = True
                flags.schedule_chime_on_flag = True
                flags.schedule_chime_off_flag = False
            if command == "chime_off" and flags.schedule_chime_off_flag is False:
                leds['chime'] = False
                flags.schedule_chime_off_flag = True
                flags.schedule_chime_on_flag = False
            if command == "tick_on" and flags.schedule_tick_on_flag is False:
                start_tick()
                flags.schedule_tick_on_flag = True
                flags.schedule_tick_off_flag = False
            if command == "tick_off" and flags.schedule_tick_off_flag is False:
                start_tick()
                flags.schedule_tick_off_flag = True
                flags.schedule_tick_on_flag = False


# --------------------------------------------------------------------
# Main Loop
# --------------------------------------------------------------------


def basic_clock():
    print('started clock')
    while True:
        start = time()
        write_time(datetime.now())
        try:
            sleep(1 + start - time())
        except:  # we get a bunch of errors.  Sometime analyse them
            # and clean this up.
            print('Skipped.  If this keeps happening, check system load')
            system(
                "logger 'Alarm clock skipped, if this keeps happening, check system\
load'")


def clock_loop():
    while 1:
        start = time()
        if flags.async_display is False:
            flags.display_lock = True
            flags.display_function()
            flags.display_lock = False
        # order of preference
        read_and_follow_schedule(schedulefile)
        check_alarm()
        check_chime()
        check_tick()
        try:
            sleep(1 + start - time())
        except:  # we get a bunch of errors.  Sometime analyse them
            # and clean this up.
            print('Skipped.  If this keeps happening, check system load')
            system(
                "logger 'Alarm clock skipped, if this keeps happening, check system\
load'")


#        raise ValueError("Testing Error") # testing

# --------------------------------------------------------------------
# Execution Starts Here
# --------------------------------------------------------------------

# prevent multiple loading (can cause conflicts)
lockfile = "/tmp/alarm-lock"
if path.isfile(lockfile):
    log('already running')
    log('if you mean to restart, please remove ' + lockfile)
    sys.exit(1)

with open(lockfile, 'w') as f:
    log('writing lock file')
    f.write(str(getpid()) + '\n')
    f.write(strftime("%D   %H:%M:%S") + '\n')

try:  # catch all errors, so we can warn the
    # user that clock has stopped

    mpd_client.mpd_alive_thread()
    restart_ntp()
    init_gpio()
    default_mode()  # init variables
    config.init()
    config.load_config()
    config.save_config(variables)  # full config file
    Thread(target=config.read_fifo_loop).start()
    grandfather_clock.set_volume(variables['hardware_volume'] + "%")

    Thread(target=input_gpio.left_watch_long_short_loop).start()
    Thread(target=input_gpio.right_watch_long_short_loop).start()
    log('started clock...')

    signal.signal(signal.SIGUSR1, signal_handler)

    clock_mode()
    chime_on()
    clock_loop()
    #basic_clock()

except Exception as e:
    write_quantity(0, 'err ')
    alarm.fallback_tones()
    log('Exception!  This shouldn\'t happen....')
    log(e)
    traceback.print_exc(file=sys.stdout)
    sys.exit(1)
