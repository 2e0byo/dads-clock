from time import strftime


def logf(filename, string):
    print(string)
    with open(filename, 'a') as f:
        f.write(strftime("%D   %H:%M:%S  ") + str(string) + '\n')


def log(message):
    logf("/opt/alarm.log", message)


debug_mode = True


def test_print(n):
    global debug_mode
    if debug_mode is True:
        print(n)
