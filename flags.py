# flag variables (only!)
# not configured by the web interface.


ChimeFlag = False  # not chiming
ChimeOn = 'Unimplemented'
AlarmFlag = False  # not ringing
AlarmOn = 'Unimplemented'
MonkFlag = False  # no monks neither...
MonkOn = False
TickFlag = False  # ...nor is it a bomb
TickOn = False
DisplayBlanked = False
functions = {
    "left_button_long": "",
    "left_button_short": "",
    "right_button_long": "",
    "right_button_short": "",
    "both_buttons": ""
}
display_function = ""
snooze = False
interactive = False
user_input = 0
interactive_upper_limit = 100
interactive_lower_limit = 0
async_display = False
display_lock = False
InteractiveAlarmFlag = False
schedule_chime_off_flag = False
schedule_chime_on_flag = False
schedule_tick_off_flag = False
schedule_tick_on_flag = False
