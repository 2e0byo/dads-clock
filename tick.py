from alarm import client
import flags
from display import leds
from variables import variables


def start_tick():
    """Set MPD playlist to 'tick' and then play 2s tick on repeat"""
    client.clear()
    client.load('tick')
    client.repeat('1')
    client.setvol(int(variables['tick_volume']))
    client.play()
    flags.TickFlag = True
    flags.TickOn = True
    leds['tick'] = True


def stop_tick():
    """Rather confusingly, this does not *disable* ticking, for that you
need 'disable_tick'.  This should be addressed"""
    client.stop()
    client.repeat('0')
    flags.TickOn = False


def disable_tick():
    """Disable ticking"""
    flags.TickFlag = False
    leds['tick'] = False
    stop_tick()


def toggle_tick():
    if leds['tick'] is False:
        start_tick()
        print('starting tick')
    else:
        disable_tick()
        print('stopping tick')
