all: display clock variables config alarm-clock util alarm flags grandfather_clock input_gpio log tick mpd_client restart_ntp

display:
	scp display.py root@ticktock.lan:~/

clock:
	scp clock.py root@ticktock.lan:~/

variables:
	scp variables.py root@ticktock.lan:~/

config:
	scp config.py root@ticktock.lan:~/

alarm-clock:
	scp alarm-clock.py root@ticktock.lan:~/

util:
	scp util.py root@ticktock.lan:~/

alarm:
	scp alarm.py root@ticktock.lan:~/

flags:
	scp flags.py root@ticktock.lan:~/

grandfather_clock:
	scp grandfather_clock.py root@ticktock.lan:~/

input_gpio:
	scp input_gpio.py root@ticktock.lan:~/

log:
	scp log.py root@ticktock.lan:~/

tick:
	scp tick.py root@ticktock.lan:~/

mpd_client:
	scp mpd_client.py root@ticktock.lan:~/

restart_ntp:
	scp restart_ntp.py root@ticktock.lan:~/
