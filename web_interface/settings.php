<?php
function make_form_field($varname, $current_value, $type, $possible_values){
    if ($type == "list"){
	printf("%s: ", ucwords($varname));
	printf("<select name=%s>\n", $varname);
	foreach( $possible_values as $val){
        if ( $val == $current_value){
            printf("<option selected='selected' value=\"%s\">%s</option>\n", $val, ucwords($val));
        }
        else {
            printf("<option value=\"%s\">%s</option>\n", $val, ucwords($val));
        }
	}
	printf("</select><br>\n");
    }
    elseif ($type == "string"){
	printf("%s: ", ucwords($varname));
	printf("<input type=\"text\" name=\"%s\" value=\"%s\"><br>\n", $varname, $current_value);
    }
    elseif ($type == "int"){
	$min = $possible_values[0];
	$max = $possible_values[1];
	printf("%s (min %s, max %s): ", ucwords($varname), $min, $max);
	printf("<input type = \"number\" name=\"%s\" min=\"%s\" max=\"%s\" value=\"%s\"><br>\n", $varname, $min, $max, $current_value);
	
    }
}

?>
<html>
    <head>
	<title>
	    TickTick Settings Page
	</title>
    </head>
    <body>
	<h1>
	    Enter Values and press 'Submit'
	</h1>
	<p>
	    Welcome to the highly functional and not-at-all fancy
	    config page!  Here you can set any variable which is
	    defined in variables.variables.  An explanation of the
	    meaning of each value is given in the <a href="./manual.pdf">manual</a></p>
	
	<?php 
	$file = fopen("/root/.clock_config", "r");
	$vars = json_decode(fgets($file), true);
	$defs = json_decode(fgets($file), true);
	fclose($file);
	?>
	<form action="submit_settings.php" method="post">
	    <?php
	    /* loop through each variable and make an entry in the form for it, preloaded with its current value */
	    foreach($vars as $var => $current_value) {
		make_form_field($var, $current_value, $defs[$var]["vtype"], $defs[$var]["values"]);
	    }
	    ?>
	    <p><input type="submit" /></p>
	</form>
	<h1>
	    Toggle Blank Display
	</h1>
	<p><form action="toggle.php" method="post">
	    <input type="hidden" name="toggle" value="blank">
	    <p><input type="submit" /></p>
	</form>
	<h1>
	    Toggle Chime
	</h1>
	<p><form action="toggle.php" method="post">
	    <input type="hidden" name="toggle" value="chime">
	    <p><input type="submit" /></p>
	</form>
	<h1>
	    Toggle Tick
	</h1>
	<p><form action="toggle.php" method="post">
	    <input type="hidden" name="toggle" value="tick">	    
	    <p><input type="submit" /></p>
	</form>
	<h1>
	    Reset to Factory Settings
	</h1>
	<p><form action="reset.php" method="post">
	    <p><input type="submit" /></p>
	</form>
    </body>
</html>