<!-- Alarm Parsing and Setting -->
<?php
$alarmfile="/opt/alarms";
if ( file_exists($alarmfile) ) {
    $raw_alarms = explode(PHP_EOL, file_get_contents($alarmfile));
}
else {
    $raw_alarms = array();
    }
?>
<html>
    <head>
	<title>
	    Edit Alarms
	</title>
    </head>
    <body>
	<h1>
	    Edit Alarms
	</h1>
	<p>Add/Remove Alarms below and then press submit:</p>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script>
	 $(function () {
	     $('#alarmForm').on('keyup keypress', "input", function(e) {
		 var keyCode = e.keyCode || e.which;
		 if (keyCode === 13) {
		     e.preventDefault();
		     return false;
		 }
	     });
	 });
	</script>
	<form action="submit_alarms.php", method="post", id="alarmForm">
	    <table id="alarmTable">
		<?php
		$i = 0;
		foreach($raw_alarms as $a){
		    $test=date_parse($a);
		    if (!$test OR !$test['year']) {
			continue;
		    }
		    $a_timestamp = strtotime($a);
		?>
		<tr id="f">
		    <td>
			On: <input type='text' name="date<?php echo $i ?>" value="<?php echo date("D M d Y",$a_timestamp); ?>">
			At: <input type='number' name='hour<?php echo $i ?>' min='0' max='23' value="<?php echo date("H",$a_timestamp);?>">:<input type='number' min='0' max='59' name='minute<?php echo $i ?>' value="<?php echo date("i",$a_timestamp);?>"></td>
			<td><input type="button" value="Delete" onclick="deleteRow(this)"></td>
		</tr>
		<?php
		$i++;
		}
		?>
	    </table>
	    <script>
	     var i = <?php echo $i; ?>;
	    </script>
	    <input type="hidden" name="total"x id="total" value="<?php echo $i; ?>">
	    <button type="button" onclick="addRow()">Add Alarm</button>
	    <input name="sub" type="submit" value="Submit Changes" />
	    <script>
	     function addRow() {
		 var table = document.getElementById("alarmTable");
		 var row = table.insertRow(0);
		 var cell = row.insertCell(0);
		 var a = new Date();
		 cell.innerHTML = newAlarm("");
		 var del = row.insertCell(1);
		 del.innerHTML = "<input type='button' value='Delete' onclick='deleteRow(this)'>"
		 i++;
		 document.getElementById('total').value=i;
	     }
	     function deleteRow(r) {
		 var i = r.parentNode.parentNode.rowIndex;
		 document.getElementById("alarmTable").deleteRow(i);
	     }
	     function newAlarm(d) {
		 if (d== "")
		     var a = new Date();
		 else
		     var a = new Date(d);
		 var e = ["On: <input type='text' name='date",i,"' value='",
			  a.toDateString(),
			  "'> At: <input type='number' min='0' max='23' name='hour",i,"' value='",
			  a.getHours().toString(),
			  "'>:<input type='number' min='0' max='59' name='minute",i,"' value='",
			  a.getMinutes().toString(),"'>"];
		 return e.join('');
	     }
	     
	    </script>

	</form>
    </body>
</html>